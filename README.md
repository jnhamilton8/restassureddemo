## What is this repository for? ##

A personal project which sets up a basic API testing framework using Rest Assured in Java and the publicly available *https://reqres.in/* API

It is used to demonstrate some of the features of Rest Assured.

## How do I run the tests? ##

The tests are written using Junit - can use Maven to execute them in the command line e.g. via `mvn test`, or for one test class,
`mvn test -Dtest=LoginTest` or simply via your IDE of choice in the usual way.

## Who do I talk to? ##

Jane Hamilton