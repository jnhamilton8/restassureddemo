package restassured.example;

import io.restassured.response.Response;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import restassured.example.environmentconfig.TestEnv;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import static restassured.example.environmentconfig.RequesAPIUtils.*;

public class LoginTest extends TestEnv {

    private JSONObject jsonBody;

    @BeforeEach
    public void beforeEach() {
        jsonBody = new JSONObject();
    }

    /**
     * Sends the post request to verify an unregistered user cannot log in
     * and verifies the response
     */
    @Test
    public void test_when_unregistered_user_attempts_to_log_in_returns_400_and_user_not_found() {
        jsonBody.put("username", "Jane");
        jsonBody.put("email", "test@test.com");
        jsonBody.put("password", "anything");

        given().log().body()
                .contentType("application/json")
                .body(jsonBody.toString())
                .when().post(LOGINPATH).then().log().all()
                .body("error", equalTo("user not found"))
                .statusCode(BAD_REQUEST);
    }

    /**
     * Sends a post login request omitting the user password and
     * ensures a bad request is received in response along with the
     * appropriate error message.
     */
    @Test
    public void test_when_logging_with_missing_password_returns_400() {
        jsonBody.put("username", "George");
        jsonBody.put("email", "george.bluth@reqres.in");
        jsonBody.put("password", "");

        given()
                .contentType("application/json")
                .body(jsonBody.toString())
                .when().post(LOGINPATH).then()
                .statusCode(BAD_REQUEST)
                .body("error", equalTo("Missing password"));
    }

    @Disabled("Disabled as just an example")
    @Test
    public void test_example_showing_auth() {
        Map<String, Object> headers = new HashMap<>();
        headers.put("header1", "value1");
        headers.put("header2", "value2");

        Response response = given().log().all(true)
                .contentType("application/json")
                .auth()
                .basic("username", "password")
                .headers(headers)
                .body("{\"name\":\"John\", \"age\":30, \"car\":null}")
                .when().log().all().post("/some/URL")
                .then()
                .statusCode(201)
                .header("header1", "value1")
                .body("$.id", not(empty())).and()
                .body("$.data.name", equalTo("Jane"))
                .extract().response();
    }
}