package restassured.example;

import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import restassured.example.environmentconfig.TestEnv;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;
import static restassured.example.environmentconfig.RequesAPIUtils.*;

public class RegisterTest extends TestEnv {

    private JSONObject jsonBody;

    @BeforeEach
    public void beforeEach() {
        jsonBody = new JSONObject();
    }

    /**
     * Sends the post request body with the user's
     * email and password to register and verifies the
     * response code and correct token is returned.
     *
     * Also demonstrates usage of logging the json request and response body
     * to output.
     */
    @Test
    public void test_only_defined_users_can_register() {
        jsonBody.put("username", "George");
        jsonBody.put("email", "george.bluth@reqres.in");
        jsonBody.put("password", "test");

        given().log().body()
                .contentType("application/json")
                .body(jsonBody.toString())
                .when().post(REGISTER).then()
                .statusCode(BAD_REQUEST)
                .body("error", equalTo("Note: Only defined users succeed registration"))
                .log().all();
    }

    /**
     * Sends a post register request omitting the user password and
     * ensures a bad request is received in response along with the
     * appropriate error message.
     */
    @Test
    public void test_if_user_omits_password_they_cannot_register() {
        jsonBody.put("username", "George");
        jsonBody.put("email", "george.bluth@reqres.in");

        given()
                .contentType("application/json")
                .body(jsonBody.toString())
                .when().post(REGISTER).then()
                .statusCode(BAD_REQUEST)
                .body("error", equalTo("Missing password"));
    }
}