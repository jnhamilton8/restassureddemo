package restassured.example;

import org.junit.jupiter.api.Test;
import restassured.example.api.RequestAPI;
import restassured.example.data.User;
import restassured.example.environmentconfig.TestEnv;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static restassured.example.environmentconfig.RequesAPIUtils.*;

public class UsersTest extends TestEnv {

    private final static int VALID_USER_ID = 2;
    private final static int INVALID_USER_ID = 23;
    private final static int RESULTS_PER_PAGE = 5;
    private final static int RESPONSE_DELAY = 3;

    /**
     * Demonstrating we can chain checks in RestAssured
     */
    @Test
    public void test_get_single_user_details_returns_200() {
        given().when().queryParam("id", VALID_USER_ID)
                .get(USERPATH).then().log().body()
                .statusCode(SUCCESS)
                .body("data.first_name", equalTo("Janet"))
                .body("data.last_name", equalTo("Weaver"));
    }

    /**
     * Demonstrating we can extract the response and assert on
     * the contents
     */
    @Test
    public void test_list_user_details_returns_200() {
                given().when()
                        .queryParam("page", "1")
                        .queryParam("per_page", RESULTS_PER_PAGE)
                        .get(USERPATH).then()
                        .statusCode(SUCCESS)
                        .body("data.id", hasItems(1, 2, 3, 4, 5))
                        .body("data.first_name", hasItems("George", "Janet", "Emma", "Eve", "Charles"))
                        .extract().response();
    }

    @Test
    public void test_get_invalid_single_user_results_in_404() {
        given().when().queryParam("id", INVALID_USER_ID)
                .get(USERPATH).then()
                .statusCode(NOT_FOUND)
                .body(equalTo("{}"));
    }

    /**
     * Can use a map to create simple json body.
     * Post action requires Jackson-databind to be in
     * pom and also scope to be set as test.
     */
    @Test
    public void test_create_a_new_user_using_map() {
        Map<String, String> newUser = new HashMap<>();
        newUser.put("name", "Jane Hamilton");
        newUser.put("job", "QA Analyst");

        given()
                .contentType("application/json")
                .body(newUser)
                .when().post(USERPATH).then()
                .statusCode(CREATED)
                .body("name", equalTo("Jane Hamilton"))
                .body("job", equalTo("QA Analyst"));
    }

    /**
     * Can use an object to create the json body.
     * Post action requires Jackson-databind to be in
     * pom and also scope to be set as test.
     */
    @Test
    public void test_create_a_new_user_using_object() {
        User user = new User("Alycia Smith", "Developer");
        given()
                .contentType("application/json")
                .body(user)
                .when().post(USERPATH).then()
                .statusCode(CREATED)
                .body("$", hasKey("id"))
                .body("name", equalTo("Alycia Smith"))
                .body("job", equalTo("Developer"));
    }

    /**
     * This test uses the RequestAPI class which abstracts
     * creation of new users.  From this we can obtain the
     * user id which is used when updating the record.
     */
    @Test
    public void test_update_an_existing_user_results_in_200() {
        //create a new user
        RequestAPI requestAPI = new RequestAPI();
        String id = requestAPI.createUser("Jason Smith", "Builder").getBody().path("id");

        //update user in API to reflect new name and job
        given()
                .pathParam("id", id)
                .contentType("application/json")
                .body("{\"name\" : \"Tim Sheldon\", \"job\": \"Consultant\"}")
                .when().put(USERPATH + "/{id}")
                .then()
                .statusCode(SUCCESS)
                .body("name", equalTo("Tim Sheldon"))
                .body("job", equalTo("Consultant"));
    }

    @Test
    public void test_delete_an_existing_user_results_in_204() {
        //create a new user
        RequestAPI requestAPI = new RequestAPI();
        String id = requestAPI.createUser("Tammy White", "Project Manager").getBody().path("id");

        //delete user
        given()
                .pathParam("id", id)
                .contentType("application/json")
                .when().delete(USERPATH + "/{id}").then()
                .statusCode(NO_CONTENT);

        //check the user really has gone
        given().pathParam("id", id)
                .when().get(USERPATH + "/{id}").then()
                .statusCode(NOT_FOUND);
    }

    /**
     * RestAssured will wait until the response is received,
     * however this demonstrates you can also assert the
     * time taken for a particular request is e.g. less
     * than a certain value
     */
    @Test
    public void test_get_multiple_user_details_successful_with_delayed_response() {
        given().when()
                .queryParam("delay", RESPONSE_DELAY)
                .get(USERPATH).then()
                .statusCode(SUCCESS)
                .body("total", equalTo(12))
                .time(lessThan(4500L));
    }
}