package restassured.example.api;

import restassured.example.data.User;
import io.restassured.response.Response;
import restassured.example.environmentconfig.RequesAPIUtils;

import static io.restassured.RestAssured.given;

/**
 * Class which abstracts API actions for easy
 * reuse within tests
 */
public class RequestAPI extends RequesAPIUtils{

    private static final String contentType = "application/json";

    public Response createUser(String aName, String aJob) {

        User user = new User(aName, aJob);

        return postMessage(user, USERPATH, contentType);
    }

    private Response postMessage(Object body, String url, String contentType){

        return given().
                body(body).
                contentType(contentType).
                when().
                post(url)
        .andReturn();
    }
}