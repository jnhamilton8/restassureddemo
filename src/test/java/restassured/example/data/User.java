package restassured.example.data;

import lombok.Data;

@Data
public class User {
    private String name;
    private String job;
    private String email;
    private String password;

    public User(String aName, String aJob) {
        this.name = aName;
        this.job = aJob;
        this.email = "";
        this.password ="";
    }
}
