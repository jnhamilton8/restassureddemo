package restassured.example.environmentconfig;

/**
 * Central collection of HTTP codes,
 * URL parameters and API endpoints for use in tests.
 */

public class RequesAPIUtils {

    //HTTP Status Codes
    public static final int SUCCESS = 200;
    public static final int NOT_FOUND = 404;
    public static final int CREATED = 201;
    public static final int NO_CONTENT = 204;
    public static final int BAD_REQUEST = 400;

    //API Endpoints
    public static final String USERPATH = "/users";
    public static final String LOGINPATH = "/login";
    public static final String REGISTER = "/register";
}