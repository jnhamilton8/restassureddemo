package restassured.example.environmentconfig;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeAll;

public class TestEnv {

    /**
     * Runs before each test class.  Sets the RestAssured
     * base.path and baseURI to be same as that passed
     * in as a system property if such exists.
     * Otherwise sets a default.
     */
    @BeforeAll
    public static void setUp() {
        var basePath = System.getProperty("base.path");
        if (basePath == null) {
            basePath = "/api";
        }
        RestAssured.basePath = basePath;

        var baseURI = System.getProperty("base.url");
        if (baseURI == null) {
            baseURI = "https://reqres.in";
        }
        RestAssured.baseURI = baseURI;
    }
}
